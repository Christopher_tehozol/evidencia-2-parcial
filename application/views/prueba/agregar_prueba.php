<!DOCTYPE html>
<html>

<?=$this->load->view('public/head','',TRUE)?>

<body class="fixed-left">
<HR width="100%">


<!-- Begin page -->
<div id="wrapper">


	<!-- Top Bar Start -->
	<div class="topbar">

		<!-- LOGO -->
		<div class="topbar-left">
			<div class="text-center">
				<a href="index.html" class="logo"><i class="icon-magnet icon-c-logo"></i><span>Ub<i class="md md-album"></i>ld</span></a>
			</div>
		</div>

		<!-- Button mobile view to collapse sidebar menu -->
		<div class="navbar navbar-default" role="navigation">

			<div class="container">
				<div class="">
					<div class="pull-left">
						<button class="button-menu-mobile open-left">
							<i class="ion-navicon"></i>
						</button>
						<span class="clearfix"></span>
					</div>

					<form role="search" class="navbar-left app-search pull-left hidden-xs">
						<input type="text" placeholder="Search..." class="form-control">
						<a href=""><i class="fa fa-search"></i></a>
					</form>


					<ul class="nav navbar-nav navbar-right pull-right">
						<li class="dropdown hidden-xs">

							<ul class="dropdown-menu dropdown-menu-lg">
								<li class="notifi-title"><span class="label label-default pull-right">New 3</span>Notification</li>
								<li class="list-group nicescroll notification-list">
									<!-- list item-->
									<a href="javascript:void(0);" class="list-group-item">
										<div class="media">
											<div class="pull-left p-r-10">
												<em class="fa fa-diamond fa-2x text-primary"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">A new order has been placed A new order has been placed</h5>
												<p class="m-0">
													<small>There are new settings available</small>
												</p>
											</div>
										</div>
									</a>

									<!-- list item-->
									<a href="javascript:void(0);" class="list-group-item">
										<div class="media">
											<div class="pull-left p-r-10">
												<em class="fa fa-cog fa-2x text-custom"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">New settings</h5>
												<p class="m-0">
													<small>There are new settings available</small>
												</p>
											</div>
										</div>
									</a>

									<!-- list item-->
									<a href="javascript:void(0);" class="list-group-item">
										<div class="media">
											<div class="pull-left p-r-10">
												<em class="fa fa-bell-o fa-2x text-danger"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">Updates</h5>
												<p class="m-0">
													<small>There are <span class="text-primary font-600">2</span> new updates available</small>
												</p>
											</div>
										</div>
									</a>

									<!-- list item-->

									<a href="javascript:void(0);" class="list-group-item">
										<div class="media">
											<div class="pull-left p-r-10">
												<em class="fa fa-user-plus fa-2x text-info"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">New user registered</h5>
												<p class="m-0">
													<small>You have 10 unread messages</small>
												</p>
											</div>
										</div>
									</a>
									<!-- list item-->
									<a href="javascript:void(0);" class="list-group-item">
										<div class="media">
											<div class="pull-left p-r-10">
												<em class="fa fa-diamond fa-2x text-primary"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">A new order has been placed A new order has been placed</h5>
												<p class="m-0">
													<small>There are new settings available</small>
												</p>
											</div>
										</div>
									</a>

									<!-- list item-->
									<a href="javascript:void(0);" class="list-group-item">
										<div class="media">
											<div class="pull-left p-r-10">
												<em class="fa fa-cog fa-2x text-custom"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">New settings</h5>
												<p class="m-0">
													<small>There are new settings available</small>
												</p>
											</div>
										</div>
									</a>
								</li>

							</ul>
						</li>

					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<!-- Top Bar End -->



	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<div class="row">
			<div class="col-sm-12">
				<div class="card-box">
					<h4 class="m-t-0 header-title"><b style="color: #000099">Registrarse</b></h4>
					<div class="row">
						<div class="col-md-12">
							<form class="form-horizontal" role="form" action="" method="post">

								<div>
									<label class="col-md-2 control-label" style="color: #0f5f9f">Nombre:</label>
									<div class="col-md-4">
										<input type="text" class="form-control" name="nombre" value="">
										<?=form_error('nombre')?>
									</div>
								</div>
								<div >
									<label class="col-md-2 control-label" style="color: #0f5f9f">Apellido paterno:</label>
									<div class="col-md-4">
										<input type="text" class="form-control" name="Apaterno"value="">
										<?= form_error('Apaterno')?>
									</div>
								</div>
								<div >
									<label class="col-md-2 control-label" style="color: #0f5f9f">Apellido materno:</label>
									<div class="col-md-4">
										<input type="text" class="form-control" name="Amaterno" value="">
										<?= form_error('Amaterno')?>
									</div>
								</div>
								<div >
									<label class="col-md-2 control-label" style="color: #0f5f9f">Edad:</label>
									<div class="col-md-4">
										<input type="text" class="form-control" name="edad" value="">
										<?=form_error('edad')?>
									</div>
								</div>
								<div>
									<label class="col-md-2 control-label" style="color: #0f5f9f">Usuario:</label>
									<div class="col-md-4">
										<input type="text" class="form-control"  name="usuario" value="">
										<?=form_error('usuario')?>
									</div>
								</div>
								<div >
									<label class="col-md-2 control-label" style="color: #0f5f9f">Contraseña:</label>
									<div class="col-md-4">
										<input type="password" class="form-control" name="password" value="">
										<?=form_error('password')?>
									</div>
									<button type="submit" class="btn btn-green waves-effect waves-light" style="color: #0b0b0b">Registrarse</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- ============================================================== -->
	<!-- End Right content here -->


















	<?=$this->load->view('public/menuleft','',TRUE)?>




</div>
<!-- END wrapper -->



<?=$this->load->view('public/base_js','',TRUE)?>




</body>
</html>
