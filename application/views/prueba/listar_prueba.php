<!DOCTYPE html>
<html>

<?=$this->load->view('public/head','',TRUE)?>

<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">


	<!-- Top Bar Start -->
	<div class="topbar">

		<!-- LOGO -->
		<div class="topbar-left">
			<div class="text-center">
				<a href="index.html" class="logo"><i class="icon-magnet icon-c-logo"></i><span>Ub<i class="md md-album"></i>ld</span></a>
			</div>
		</div>

		<!-- Button mobile view to collapse sidebar menu -->
		<div class="navbar navbar-default" role="navigation">
			<div class="container">
				<div class="">
					<div class="pull-left">
						<button class="button-menu-mobile open-left">
							<i class="ion-navicon"></i>
						</button>
						<span class="clearfix"></span>
					</div>

					<form role="search" class="navbar-left app-search pull-left hidden-xs">
						<input type="text" placeholder="Search..." class="form-control">
						<a href=""><i class="fa fa-search"></i></a>
					</form>


					<ul class="nav navbar-nav navbar-right pull-right">
						<li class="dropdown hidden-xs">

							<ul class="dropdown-menu dropdown-menu-lg">
								<li class="notifi-title"><span class="label label-default pull-right">New 3</span>Notification</li>
								<li class="list-group nicescroll notification-list">
									<!-- list item-->
									<a href="javascript:void(0);" class="list-group-item">
										<div class="media">
											<div class="pull-left p-r-10">
												<em class="fa fa-diamond fa-2x text-primary"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">A new order has been placed A new order has been placed</h5>
												<p class="m-0">
													<small>There are new settings available</small>
												</p>
											</div>
										</div>
									</a>

									<!-- list item-->
									<a href="javascript:void(0);" class="list-group-item">
										<div class="media">
											<div class="pull-left p-r-10">
												<em class="fa fa-cog fa-2x text-custom"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">New settings</h5>
												<p class="m-0">
													<small>There are new settings available</small>
												</p>
											</div>
										</div>
									</a>

									<!-- list item-->
									<a href="javascript:void(0);" class="list-group-item">
										<div class="media">
											<div class="pull-left p-r-10">
												<em class="fa fa-bell-o fa-2x text-danger"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">Updates</h5>
												<p class="m-0">
													<small>There are <span class="text-primary font-600">2</span> new updates available</small>
												</p>
											</div>
										</div>
									</a>

									<!-- list item-->

									<a href="javascript:void(0);" class="list-group-item">
										<div class="media">
											<div class="pull-left p-r-10">
												<em class="fa fa-user-plus fa-2x text-info"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">New user registered</h5>
												<p class="m-0">
													<small>You have 10 unread messages</small>
												</p>
											</div>
										</div>
									</a>
									<!-- list item-->
									<a href="javascript:void(0);" class="list-group-item">
										<div class="media">
											<div class="pull-left p-r-10">
												<em class="fa fa-diamond fa-2x text-primary"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">A new order has been placed A new order has been placed</h5>
												<p class="m-0">
													<small>There are new settings available</small>
												</p>
											</div>
										</div>
									</a>

									<!-- list item-->
									<a href="javascript:void(0);" class="list-group-item">
										<div class="media">
											<div class="pull-left p-r-10">
												<em class="fa fa-cog fa-2x text-custom"></em>
											</div>
											<div class="media-body">
												<h5 class="media-heading">New settings</h5>
												<p class="m-0">
													<small>There are new settings available</small>
												</p>
											</div>
										</div>
									</a>
								</li>

							</ul>
						</li>

					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<!-- Top Bar End -->
	<!-- Start content -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container">

				<!-- Page-Title -->
				<div class="row">
					<div class="col-sm-12">
						<h4 class="page-title">Datatable</h4>
						<ol class="breadcrumb">
							<li>
								<a href="#">Ubold</a>
							</li>
							<li>
								<a href="#">Tables</a>
							</li>
							<li class="active">
								Datatable
							</li>
						</ol>
					</div>
				</div>



				<div class="row">
					<div class="col-sm-12">
						<div class="card-box">
							<table id="datatable" class="table table-striped table-bordered">
								<thead>
								<tr>
									<th>ID</th>
									<th>NOMBRE</th>
									<th>APELLIDO PATERNO</th>
									<th>APELLIDO MATERNO</th>
									<th>EDAD</th>
									<th>USUARIO</th>

								</tr>
								</thead>


								<tbody>
								<?php foreach ($persona1 as $key => $persona){?>
									<tr>
										<td>
										<?= $persona->idPersona ?>
										</td>
										<td>
											<?= $persona->Nombre ?>
										</td>
										<td>
											<?= $persona->Apaterno?>

										</td>
										<td><?= $persona->Amaterno?>
										</td>

										<td><?= $persona->Edad?>
										</td>

										<td><?= $persona->Usuario?></td>

										<td class="text-right">
											<a href="prueba/editar/<?=$persona->idPersona?>">Editar</a>

										</td>
										<td class="text-right">
											<a href="prueba/eliminar/<?=$persona->idPersona?>">Eliminar</a>

										</td>
									</tr>
								<?php }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>


			</div> <!-- container -->

		</div> <!-- content -->

		<footer class="footer">
			2015 © Ubold.
		</footer>

	</div>

	<?=$this->load->view('public/menuleft','',TRUE)?>




</div>
<!-- END wrapper -->



<?=$this->load->view('public/base_js','',TRUE)?>




</body>
</html>
