<?php

defined ('BASEPATH') OR exit ('No direct script access allowed');

/**Modelo para alumnos
 *Aqui tendremos informacion e insertaremos informacion de los alumnos
 * @author Eduardo Salazar Santos
 * */


class Mprueba extends CI_Model
{
	/**
	 * *lISTAR TODOS LOS ALUMNOS
	 * @RETURNS Array
	 */

	public function listar_alumnos()
	{
		return $this->db->get('persona')->result();
	}

	/**
	 * *MUESTRA ALUMNOS
	 * @RETURNS Array
	 */

	public function obtener_alumno($id)
	{

		$this->db->where('idPersona',$id);
		return $this->db->get('persona')->row();
	}

	public function guardar_datos($data)
	{
		return $this->db->insert('persona', $data);
	}

	/**
	 * ELIMINAR ALUMNO
	 * @RETURNS Array
	 */

	public function actualizar_datos($data,$id)
	{
		$this->db->where('idPersona',$id);
		return $this->db->update('persona',$data);
	}

	public function eliminar_datos($id)
	{
		$this->db->where('idPersona',$id);
		return $this->db->delete('persona');
	}


}
