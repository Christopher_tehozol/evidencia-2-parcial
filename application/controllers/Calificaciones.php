<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calificaciones extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('prueba/listar_prueba');
	}

	public function eliminar()
	{
		$this->load->view('prueba/delete_prueba');
	}

	public function editar()
	{
		$this->load->view('prueba/editar_prueba');
	}


	public function listar()
	{
		$this->load->view('prueba/listar_prueba');
	}
}
