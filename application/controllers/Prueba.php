<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class prueba extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Mprueba');
	}

	public function index()
	{
		$data['persona1'] = $this->Mprueba->listar_alumnos();
		$this->load->view('prueba/listar_prueba',$data);
	}

	public function eliminar($id = null)
	{
		$this->alertas->db($this->Mprueba->eliminar_datos($id), 'prueba');
	}

	public function agregar()
	{

		$this->form_validation->set_rules(
			array(
				array(
					'field' => 'nombre',
					'label' => 'Nombre de la persona',
					'rules' => 'required'
				),
				array(
					'field' => 'Apaterno',
					'label' => 'Apellido paterno de la persona',
					'rules' => 'required'
				),
				array(
					'field' => 'Amaterno',
					'label' => 'Apellido materno de la persona',
					'rules' => 'required'
				),
				array(
					'field' => 'edad',
					'label' => 'Edad de la persona',
					'rules' => 'required'
				),
				array(
					'field' => 'usuario',
					'label' => 'Usuario de la persona',
					'rules' => 'required'
				),
				array(
					'field' => 'password',
					'label' => 'Pasword de la persona',
					'rules' => 'required'
				)
			)
		);

		if($this->form_validation->run() && $this->input->post()){
			$data = array(
				'Nombre'=> $this->input->post('nombre'),
				'Apaterno'=> $this->input->post('Apaterno'),
				'Amaterno'=> $this->input->post('Amaterno'),
				'Edad'=> $this->input->post('edad'),
				'Usuario'=> $this->input->post('usuario'),
				'Password'=> $this->input->post('password')
			);
			$this->alertas->db($this->Mprueba->guardar_datos($data), 'prueba/agregar');
		}



		$this->load->view('prueba/agregar_prueba');

	}

	public function editar($id=null)
	{
		$data['persona'] = $this->Mprueba->obtener_alumno($id);


		$this->form_validation->set_rules(
			array(
				array(
					'field' => 'nombre',
					'label' => 'Nombre de la persona',
					'rules' => 'required'
				),
				array(
					'field' => 'Apaterno',
					'label' => 'Apellido paterno de la persona',
					'rules' => 'required'
				),
				array(
					'field' => 'Amaterno',
					'label' => 'Apellido materno de la persona',
					'rules' => 'required'
				),
				array(
					'field' => 'edad',
					'label' => 'Edad de la persona',
					'rules' => 'required'
				),
				array(
					'field' => 'usuario',
					'label' => 'Usuario de la persona',
					'rules' => 'required'
				),
				array(
					'field' => 'password',
					'label' => 'Pasword de la persona',
					'rules' => 'required'
				)
			)
		);

		if($this->form_validation->run() && $this->input->post()){
			$data = array(
				'Nombre'=> $this->input->post('nombre'),
				'Apaterno'=> $this->input->post('Apaterno'),
				'Amaterno'=> $this->input->post('Amaterno'),
				'Edad'=> $this->input->post('edad'),
				'Usuario'=> $this->input->post('usuario'),
				'Password'=> $this->input->post('password')
			);
			$this->alertas->db($this->Mprueba->actualizar_datos($data,$id), 'prueba');
		}


		$this->load->view('prueba/editar_prueba',$data);
	}


	public function listar()
	{
		$this->load->view('prueba/listar_prueba');
	}
}
